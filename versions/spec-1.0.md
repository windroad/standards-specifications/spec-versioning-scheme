# Windroad Versioning Scheme Specification 1.0

## Format

A windroad version string comprises four parts: **version number** (denoted by `VERSION`),
**patch level** (denoted by `PATCH`), **stage indicator** (denoted by `STAGE`) and **build ID** (denoted by `BUILD`) and is constructed in the following order:

```
VERSION.PATCH.STAGE.BUILD
```

**Version number** is calculated from `MAJOR` and `MINOR` numbers with the formula:

```
MAJOR * 100 + MINOR
```
Note: `MINOR` must be in range of 0 and 99.

**STAGE** is a string derived from one of the following patterns:

|Pattern|Description|Weight|
|-------|-----------|------|
|A|Alpha version|0
|B|Beta version|10
|IR`N`|Internal Release (`N` [0, 99], optional)|200+`N`
|PR`N`|Public Release (`N` [0, 99], optional)|300+`N`
|RC`N`|Release Candidate (`N` [0, 99], optional)|100+`N`

## Augmented Backus–Naur Form Grammar for Valid Windroad Versions

```abnf
;; A Windroad Version
wr-version = version "." patch "." stage "." build

;; version number
version = [ major ] minor
major = digit1-9 *DIGIT
minor = DIGIT DIGIT

;; patch level
patch = '0' / 1*DIGIT

;; stage indicator
stage = "A" / "B" / ("IR" [ *DIGIT ]) / ("PR" [ *DIGIT ]) / ("RC" [ *DIGIT ])

;; build version
build = digit1-9 *DIGIT

;; misc.
digit1-9 = "1" / "2" / "3" / "4" / "5" / "6" / "7" / "8" / "9"
DIGIT = "0" / digit1-9
```

## Principles

1. Only increment the `MAJOR` version for: (a) Marketing purposes, (b) incompatible changes, and (c) new features.
2. Only increment the `MINOR` version for: functionality in a backwards compatible manner.
3. Only increment the `PATCH` version for: backward compatible bug fixes.
4. `MINOR`, `PATCH` and `BUILD` must be reset to 0, when `MAJOR` is incremented.
5. `PATCH` must be reset to 0, when `MINOR` is incremented.

## Precedence of a Windroad Version

1. Precedence is calculated by separating the version string into `VERSION`, `PATCH` and `STAGE` in that order (`BUILD` does not figure into precedence).
2. Precedence is determined by the first difference when comparing each of these
   identifiers from left to right as follows: `VERSION` and `PATCH` are always compared numerically.

   ```
   92.0.A.0 < 92.1.A.0 < 192.9.A.0 < 192.10.A.0
   ```

3. When `VERSION` and `PATCH` are equal, the precedence is then determined by the weight
   of `STAGE` indicator.

   ```
   9.0.A.0 < 9.0.B.0 < 9.0.IR.0 < 9.0.PR.0
   ```

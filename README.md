# Windroad Software Versioning Scheme

## Overview

This repo contains specification documents of Windroad Software Versioning.
This versioning scheme is used throughout all projects from Windroad.

**Latest version: [1.0](versions/spec-1.0.md)**

## Implementation

The utility **[vertool](https://gitlab.com/windroad/orbis-sdi/vertool)** from Orbis SDI package provides functionalities to manage project versions.
